/* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

public class FirstBadVersion {
	public int firstBadVersion(int n) {
		int left = 1, right = n;
		while (left < right) {
			int center = left + (right - left) / 2;
			if (isBadVersion(center)) {
				right = center;
			} else {
				left = center + 1;
			}
		}
		return left;
	}
}
